/**
 * Url para publicaciones
 */
export const API_PUBLICATION_POST = '/publication/create'
export const API_PUBLICATION      = '/publication'