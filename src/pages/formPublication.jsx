import { Alert, Button, Container, Stack, TextField } from '@mui/material'
import { useFormik } from 'formik'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { savePublication } from '../actions/publication'
import AppBarPage from '../components/appBar'

const FormPublication = () => {

    const dispatch = useDispatch();
    const [ severity, setSeverity ] = useState('');
    const [ message, setMessage ] = useState('');

    const validate = (values) => {
        let errors= {};

        if(!values.title){errors.title='El titulo es requerido'}
        if(!values.author){errors.author='El autor es requerido'}
        if(!values.content){errors.content='El contenido es requerido'}

        return errors

    }

    const formik = useFormik({
        initialValues:{
            title:'',
            author:'',
            content:''
        },
        validate,
        onSubmit: values => handleSubmit(values)
    })

    const handleSubmit = async (values) => {
        const response = await dispatch(savePublication(values))
        setMessage(response.message);
        setSeverity(response.severity);

        if(response.severity==='success'){
            formik.values.title=''
            formik.values.author=''
            formik.values.content=''
        }
    } 

    return (
        <Container fixed>
            <AppBarPage />
            <div style={stylesComponents.styleDiv}>
                <form onSubmit={formik.handleSubmit}>
                    <Stack spacing={2}>
                        {!message ? null : <Alert severity={severity}>{message}</Alert>}
                        <TextField 
                            id='title'
                            label='Titulo'
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.title}
                            helperText={formik.touched.title ?  formik.errors.title : ''}
                            error={formik.touched.title && formik.errors.title} 
                            />
                        <TextField 
                            id='author'
                            label='Autor'
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.author}
                            helperText={formik.touched.author ?  formik.errors.author : ''}
                            error={formik.touched.author && formik.errors.author} 
                            />
                        <TextField 
                            id='content'
                            label='Contenido'
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.content}
                            multiline
                            rows={5}
                            helperText={formik.touched.content ?  formik.errors.content : ''}
                            error={formik.touched.content && formik.errors.content} 
                            />

                        <Button type='submit' variant='contained'> Enviar </Button>
                    </Stack>
                </form>
            </div>
        </Container>
    )
}

export default FormPublication

const stylesComponents = {
    styleDiv:{
        paddingTop:'10px',
    },
}