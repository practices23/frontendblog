import { Button, Card, CardContent, CardHeader, Container } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { getPublicationByID } from '../actions/publication';
import { Link as RouterLink, useParams } from 'react-router-dom';

const Publication = () => {
    const ref = useParams();
    const dispatch = useDispatch(); 
    
    const [data, setData] = useState([]);

    useEffect(() => {
        getPublication()
    }, [])

    const getPublication = async () => {
        const response = await dispatch(getPublicationByID(ref.id));
        if(response.severity==='success'){
            setData(response.data);
        }
    }
    

    return (
        <Container style={{paddingTop:'50px'}} fixed>
            {!data ? null :
            <Card>
                <CardHeader 
                    title={data.title}
                    subheader={data.author}
                    action={
                        <Button component={RouterLink} to={'/dashboard'} variant='outlined'>atras</Button>
                    }
                    />
                <CardContent>
                    {data.content}
                </CardContent>
            </Card>
            }
        </Container>
    )
}

export default Publication