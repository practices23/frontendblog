import AppBarPage from '../components/appBar';
import { useDispatch } from 'react-redux';
import { getPublication } from '../actions/publication';
import { Link as RouterLink, useLocation } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import {
    Button,
    Container,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    Typography,
} from '@mui/material';
import moment from "moment";
import 'moment/locale/es'
moment.locale('es');



const IndexPage = () => {

    const [list, setList] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        getAllPublications()
    }, [])

    const getAllPublications = async () =>{
        const response = await dispatch(getPublication());

        if(response.severity==='success'){
            setList(response.data);

        }
    }
    const Contenido = ({cont})=> {
            // return 'myArray';
            const myArray = cont.substr(0, 70)
            return myArray;
    }
    

    return (
        <Container fixed>
            <AppBarPage />
            <div style={stylesComponents.styleDiv}> 
                {list.length === 0 ? 
                    <Typography> no hay datos en la base </Typography>
                :
                    <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                        {list.map((index)=> (
                            <ListItem alignItems="flex-start">
                                <ListItemButton
                                    component={RouterLink} to={`/dashboard/publication/${index._id}`}
                                    >
                                    <ListItemText
                                    primary={ 
                                        <React.Fragment>
                                            <Typography
                                                sx={{ display: 'inline' }}
                                                component="span"
                                                variant="body2"
                                                color="text.primary"
                                            >
                                                {index.title}
                                            </Typography>
                                            - {moment(index.createdAt).format('dddd Do MMMM YYYY, h:mm:ss') } 
                                            </React.Fragment>}
                                    secondary={
                                        <React.Fragment>
                                            <Typography
                                                sx={{ display: 'inline' }}
                                                component="span"
                                                variant="body2"
                                                color="text.primary"
                                            >
                                                {index.author}
                                            </Typography>
                                            - { <Contenido cont={index.content} /> }
                                        </React.Fragment>
                                    }
                                    />
                                </ListItemButton>
                            </ListItem>

                        ))}
                    </List>
                }

            </div>

        </Container>

    )
}

export default IndexPage

const stylesComponents = {
    styleDiv:{
        paddingTop:'10px',
    },
}