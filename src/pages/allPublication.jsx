import { Button, Container, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { getPublication } from '../actions/publication';
import AppBarPage from '../components/appBar'
import { TablePublication } from '../components/tablePublication';

const AllPublication = () => {
    const [list, setList] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        getAllPublications()
    }, [])

    const getAllPublications = async () =>{
        const response = await dispatch(getPublication());

        if(response.severity==='success'){
            setList(response.data);

        }
    }

    return (
        <Container fixed>
            <AppBarPage />
            {list.length === 0 ? 
                    <Typography> no hay datos en la base </Typography>
                :
                <TablePublication row={list} />
            }
        </Container>
    )
}

export default AllPublication