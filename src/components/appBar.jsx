import { AppBar, Box, Button, Container, Stack } from '@mui/material';
import * as React from 'react';
import { Link as RouterLink, useLocation } from 'react-router-dom';



    const AppBarPage = ()=> {

    const id = useLocation();

        return (
            <AppBar position="static">
                <Container fixed>
                    <Box sx={{ flexGrow: 1,}}>
                        <Stack direction='row' spacing={2}>
                            <Button style={styleButton([id,1])} to="/" component={RouterLink} >Inicio</Button>
                            <Button style={styleButton([id,2])} to="/dashboard/form" component={RouterLink} >Nueva Publicacion</Button>
                            <Button style={styleButton([id,3])} to="/dashboard/allpublication" component={RouterLink} >Todas las Publicaciones</Button>
                        </Stack>
                    </Box>

                </Container>
            </AppBar>
        );
    }
    
    export default AppBarPage;

    const styleButton = (id) => {

        if(id[0].pathname=== '/dashboard' && id[1]===1){
            return {
                color:'#000', 
                backgroundColor:'#fff'
            }
        }else if(id[0].pathname=== '/dashboard/form' && id[1]===2){
            return {
                color:'#000', 
                backgroundColor:'#fff'
            }
        }else if(id[0].pathname=== '/dashboard/allpublication' && id[1]===3){
            return {
                color:'#000', 
                backgroundColor:'#fff'
            }
        }else{
            return {
                color:'#fff' 
    
            }
        }

        
    }