/**
 * tabla para mostrar todos los tickets 
 */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { visuallyHidden } from "@mui/utils";
import FilterListIcon from '@mui/icons-material/FilterList';
import moment from "moment";
import {
    Paper,
    Table,
    TableRow,
    TableHead,
    TableBody,
    TableSortLabel,
    TableCell,
    TableContainer,
    TablePagination,
    InputAdornment,
    Box,
    TextField,
} from '@mui/material';

import 'moment/locale/es'

moment.locale('es');


    function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
    }

    function getComparator(order, orderBy) {
    return order === "desc"
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
    }

    // This method is created for cross-browser compatibility, if you don't
    // need to support IE11, you can use Array.prototype.sort() directly
    function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) {
        return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
    }

    const headCells = [
        {
            title: "description",
            numeric: false,
            label: "Titulo"
        },
        {
            id: "author",
            numeric: false,
            label: "Autor"
        },
        {
            id: "content",
            numeric: false,
            label: "Contenido"
        },
        {
            id: "createdAt",
            numeric: false,
            label: "Fecha"
        }
    ];

    function EnhancedTableHead(props) {

        const { order, orderBy, onRequestSort } = props;
        const createSortHandler = (property) => (event) => {
            onRequestSort(event, property);
        };

        return (
            <TableHead>
                <TableRow>
                    {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                        active={orderBy === headCell.id}
                        direction={orderBy === headCell.id ? order : "asc"}
                        onClick={createSortHandler(headCell.id)}
                        >
                        {headCell.label}
                        {orderBy === headCell.id ? (
                            <Box component="span" sx={visuallyHidden}>
                            {order === "desc" ? "sorted descending" : "sorted ascending"}
                            </Box>
                        ) : null}
                        </TableSortLabel>
                    </TableCell>
                    ))}
                    <TableCell />
                </TableRow>
                </TableHead>
        );
    }

    EnhancedTableHead.propTypes = {
        onRequestSort: PropTypes.func.isRequired,
        order: PropTypes.oneOf(["asc", "desc"]).isRequired,
        orderBy: PropTypes.string.isRequired,
        rowCount: PropTypes.number.isRequired
    };

    export function TablePublication( props ) {

        const [order, setOrder] = useState("desc");   //ordena de forma descendiente los tickets (mas reciente al mas antiguo)
        const [orderBy, setOrderBy] = useState("createdAt");  //columna de la cual se quiere ordenar
        const [page, setPage] = useState(0);
        const [rowsPerPage, setRowsPerPage] = useState(5);

        const [arreglo, setArreglo] = React.useState([]); //Se genera areglo copia para hacer el filtrado de informacion. Datos obtenidos por props de su respectivo componente
        const [rows, setRows] = React.useState([]);       //Arreglo original es lo que mostrara la tabla. Datos obtenidos por props de su respectivo componente
        const [content, setContent] = React.useState(''); 
        const [title, setTitle] = React.useState(''); 
        const [author, setAuthor] = React.useState(''); 

        useEffect(() => {
            
            setRows(props.row)
            setArreglo(props.row)


        }, [props])

        if(props.row.length !== 0 && rows.length === 0){
            setRows(props.row)
            setArreglo(props.row)
        }
            

        const handleRequestSort = (event, property) => {
            const isAsc = orderBy === property && order === "asc";
            setOrder(isAsc ? "desc" : "asc");
            setOrderBy(property);
        };

        const handleChangePage = (event, newPage) => {
            setPage(newPage);
        };

        const handleChangeRowsPerPage = (event) => {
            setRowsPerPage(parseInt(event.target.value, 10));
            setPage(0);
        };


        const filterTitle = event => {
            var text = event.target.value;
            const data = arreglo;
            const newData = data.filter(function(item) {
                return item.title.toUpperCase().indexOf(text.toUpperCase()) > -1 ;
            });

            setRows(newData);
            setTitle(text);
        };

        const filterAuthor = event => {
            var text = event.target.value;
            const data = arreglo;
            const newData = data.filter(function(item) {
                return item.author.toUpperCase().indexOf(text.toUpperCase()) > -1 ;
            });

            setRows(newData);
            setAuthor(text);
        };

        const filterContent = event => {
            var text = event.target.value;
            const data = arreglo;
            const newData = data.filter(function(item) {
                return item.content.toUpperCase().indexOf(text.toUpperCase()) > -1 ;
            });

            setRows(newData);
            setContent(text);
        };
        
    return (
        <Box sx={{ width: "100%" }}>
            <Paper sx={{ width: "100%", mb: 2 }}>
                <TableContainer component={Paper}>
                    <Table aria-label="collapsible table">
                        <EnhancedTableHead
                        order={order}
                        orderBy={orderBy}
                        onRequestSort={handleRequestSort}
                        rowCount={props.row.length}
                        />
                        <TableCell> 
                            <TextField  
                                value={title}
                                onChange={filterTitle}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <FilterListIcon />
                                        </InputAdornment>
                                    )
                                }}
                                variant="standard"/>
                        </TableCell>
                        <TableCell>
                            <TextField  
                                value={author}
                                onChange={filterAuthor}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <FilterListIcon />
                                        </InputAdornment>
                                    )
                                }}
                                variant="standard"/>
                        </TableCell>
                        <TableCell>
                            <TextField  
                                value={content}
                                onChange={filterContent}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <FilterListIcon />
                                        </InputAdornment>
                                    )
                                }}
                                variant="standard"/>
                        </TableCell>
                        <TableCell/>

                        <TableBody>

                        {stableSort(rows, getComparator(order, orderBy))
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row, index) => {
                            return (
                                    <TableRow
                                    hover
                                    key={index}
                                    >
                                    <TableCell>{row.title}</TableCell>
                                    <TableCell>{row.author}</TableCell>
                                    <TableCell>{row.content}</TableCell>
                                    <TableCell>{moment(row.createdAt).format('dddd Do MMMM YYYY, h:mm:ss')}</TableCell>
                                </TableRow>
                                
                            );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={props.row.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
        </Box>
    );
}


