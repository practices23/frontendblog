import BlogAxios from "../config/axiosBlog";
import { API_PUBLICATION, API_PUBLICATION_POST } from "../utils/urlWebServices";

/**
*
* Guardar un categoria, tema y subtema de un Ticket
*
* @param  {Array} data Arreglo con los datos necesarios para hacer petición a API
*
* @return   
*
 */
export function getPublication(){

    return async () => {

        try {
            
            const response = await BlogAxios.get(API_PUBLICATION);
            

            if( response.data.code === 200 ){
                return {
                    message: 'Datos de la base de conocimiento obtenidos correctamente', 
                    severity: 'success',
                    data: response.data.data
                };
            } 

            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };


        } catch (error) {

            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };

        }
    }
}

/**
*
* Guardar un categoria, tema y subtema de un Ticket
*
* @param  {Array} data Arreglo con los datos necesarios para hacer petición a API
*
* @return   
*
 */
export function savePublication(data){

    return async () => {

        try {
            
            const response = await BlogAxios.post(API_PUBLICATION_POST, data);
            
            if( response.status === 201 ){
                return {
                    message: 'Publicacion guardada correctamente', 
                    severity: 'success'
                };
            } 

            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };


        } catch (error) {

            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };

        }
    }
}


/**
*
* Guardar un categoria, tema y subtema de un Ticket
*
* @param  {Array} data Arreglo con los datos necesarios para hacer petición a API
*
* @return   
*
 */
export function getPublicationByID(data){

    return async () => {

        try {
            
            const response = await BlogAxios.get(`${API_PUBLICATION}/${data}`);
            

            if( response.data.code === 200 ){
                return {
                    message: 'Datos de la base de conocimiento obtenidos correctamente', 
                    severity: 'success',
                    data: response.data.data
                };
            } 

            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };


        } catch (error) {

            return {
                message: 'Error interno, Favor de intentar mas tarde', 
                severity: 'error'
            };

        }
    }
}
