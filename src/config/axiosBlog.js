import axios from 'axios'

const BlogAxios = axios.create({
    // baseURL: process.env.REACT_APP_BACKEND_API_URL,
    baseURL:'http://localhost:8080/api',

})

export default BlogAxios

