import React from 'react';

import IndexPage from './pages/IndexPage';
import FormPublication from './pages/formPublication';
import Page404 from './pages/page404';
import AllPublication from './pages/allPublication';

import {
    Routes,
    Route,
    Navigate
} from 'react-router-dom';
import Publication from './pages/publication';

export default function Router() {

    return(
        <Routes>
            <Route path="/dashboard" element={<IndexPage />} />
            <Route path="/dashboard/form" element={<FormPublication/>} />
            <Route path="/dashboard/publication/:id" element={<Publication/>} />
            <Route path="/dashboard/allpublication" element={<AllPublication/>} />
            <Route path="/404" element={<Page404 />} />
            <Route path="/" element={<Navigate to="/dashboard" />} />
            <Route path="*" element={<Page404 />} />
        </Routes>
    )
}
